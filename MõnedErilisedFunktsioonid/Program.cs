﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Collections;

namespace MõnedErilisedFunktsioonid
{
    class A { }
    class B : A { }
    class C : B { }
    class D : C { }

    class Ahaa : IEnumerable<string>
    {
        IEnumerator<string> IEnumerable<string>.GetEnumerator()
        {
            return new EnumAhaa();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new EnumAhaa();
        }
    }

    class EnumAhaa : IEnumerator<string>
    {
        string s;

        string IEnumerator<string>.Current
        { get { return s; } }

        object IEnumerator.Current  => s;

       

        void IDisposable.Dispose()
        {
            ;
        }

        bool IEnumerator.MoveNext()
        {
            Console.Write("anna nimi: ");
            return ((s = Console.ReadLine()) != "");
        }

        void IEnumerator.Reset()
        {
           // Console.WriteLine("alustame") ;
        }
    }


    static class E
    {
        public static IEnumerable<int> Millised (this IEnumerable<int> m, Func<int, bool> f)
        {
            foreach (var x in m) if (f(x)) yield return x;
        }

        public static bool KasOnPaaris(int x) { return x % 2 == 0; }


        public static IEnumerable<string> Teretamine()
        {
            string s = "*";
            do
            {
                Console.Write("Anna nimi: ");
                s = Console.ReadLine();
                if (s != "") yield return s;
            } while (s != "");
        }

        public static string Trüki(this String x)
        {
            Console.WriteLine("{0} - kell {1}", x, DateTime.Now.ToLongTimeString());
            return x;
        }

        public static IEnumerable<int> Paaris(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
            {
                if (x % 2 == 0) yield return x;
            }
        }
        public static IEnumerable<int> Suured(this IEnumerable<int> mass)
        {
            foreach (var x in mass)
            {
                if (x > 4) yield return x;
            }
        }



    }



    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 87, 9 };

            foreach(var x in arvud
                .Where( x => x > 4)
                .Where(x => x % 2 == 0)
                .OrderBy(x => x%3)
                )
                Console.WriteLine(x);

// Language integrated query - LINQ
            foreach (var y in (
                from x in arvud
                where x > 4
                where x % 2 == 0
                orderby x % 3
                select x))

                Console.WriteLine(y);






            
        }

    }

    class Proov
    {
        public string Nimi;
        public Proov(string nimi) { Nimi = nimi; }

        public static void TeeMidagi(Proov x)
        {
            Console.WriteLine("teeme midagi {0}", x.Nimi);
        }

        public Proov TeeMidagi()
        {
            Console.WriteLine("teeme midagi {0}", this.Nimi);
            return this;
        }
    }


}
